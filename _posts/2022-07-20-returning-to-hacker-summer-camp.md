---
layout: post
title: "Returning to Hacker Summer Camp"
category: Security
date: 2022-07-20
tags:
  - DEF CON
  - Hacker Summer Camp
---
It's that time of year again -- Hacker Summer Camp.  (Hacker Summer Camp is the
~weeklong period where several of the largest hacker/information security
conferences take place in Las Vegas, NV, including DEF CON and Black Hat USA.)
This will be the 3rd year in a row where it takes place under the spectre of a
worldwide pandemic, and the first one to be fully in-person again.
[BSidesLV](https://bsideslv.org/) has returned to in-person, [DEF
CON](https://defcon.org/html/defcon-30/dc-30-index.html) is in-person only,
[Black Hat](https://www.blackhat.com/) will be in full swing, and
[Ringzer0](https://ringzer0.training/) will be offerring in-person trainings.
It's *almost* enough to forget there's still an ongoing pandemic.

I did attend last year's hybrid DEF CON in person, and I've been around a few
times, so I wanted to share a few tidbits, especially for first timers.
Hopefully it's useful to some of you.

<!--more-->

* Table of Contents
{:toc}

## Conferences/Events

- [DEF CON](https://defcon.org/) is arguably the penultimate event of the week.
  By far the largest by attendance, it also brings the greatest variety in
  hackers to the event.  Ranging from students just getting into the scene to
  seasoned hackers with decades of experience to industry professionals, the
  networking opportunities are limitless.  The talks are generally high quality,
  though they can be a bit of a mixed bag sometimes.  Some will
  teach/demonstrate great things, and I always find a few worth watching, even
  if only when they get published on YouTube.
  
  There are "villages" for every topic and space -- voting machines, hardware
  hacking, Red Teaming, IoT, lockpicking, social engineering, and more.  The
  villages allow niche areas of hacking to showcase their special interests, and
  are generally run by individuals with a pure passion for their field.  If you
  want to know more about a particular subfield of hacking, there is no better
  way than finding the right village.
  
  For the more competitive type, there's a variety of competitions.  In addition
  to the main "DEF CON CTF", there's also typically smaller CTFs in the Contest
  area or individual villages, so those looking for a challenge can put their
  skills to the test.  Other competitions in the past have included a scavenger
  hunt, a password cracking competition, a beverage cooling competition, and
  more.
  
  In the evening, there's variety of activities from parties/concerts to "Hacker
  Jeopardy" -- a very mature take on Jeopdardy! with a hacker theme.  There's
  also plenty of private parties and places to hang out with fellow hackers all
  evening long.
  
  You may also hear people refer to "the badge" when talking about admission to
  the conference.  While other conferences usually talk about registration or a
  ticket and have some boring piece of paper to present as your admission, DEF
  CON badges have become a work of art.  Approximately every other year, the
  badge is electronic and has microcontrollers and some electronic function.  In
  theory, DEF CON 30 should be a "passive" year, the creators of the badge (MK
  Factor) have [confirmed that it will be electronic this
  year](https://www.youtube.com/watch?v=bceoYgzvGu8).  (Check out the linked
  interview if you're curious.)
  
  New this year is [DEFCON
  trainings](https://training.defcon.org/collections/all).  These are taking
  place *after* DEF CON and providing some opportunities to get high-quality
  training associated with the conference.  They're all 2-day trainings, but
  they appear to be a good value for money in comparison to many other
  commercial training offerings.
  
- [Black Hat](https://www.blackhat.com/us-22/) is the premiere security industry
  conference.  I differentiate it from a hacking conference in that most of the
  people who are there will be people who strictly work in the industry and far
  fewer who are hackers just for the fun of it.  Part of this is the cost (at
  least an order of magnitude more than DEF CON), and
  part of this is the general atmosphere.  Polo shirts are the order of the day
  instead of black t-shirts and mohawks.
  
  There's lots of high-quality technical material, but also a vendor sales floor
  with all the sales pitches you can possibly imagine.  (But this is also where
  you can get free SWAG and party invites, so it's not all terrible news.)
  
  Black Hat also has a multitude of training opportunities.  In fact, Black Hat
  USA is likely the largest single site training event for the information
  security space each year.  There's trainings for every background and skill
  level, for all kinds of specialities, and in both 2- and 4-day formats.
  
- [BSidesLV](https://bsideslv.org/) is the B-Side to Black Hat.  A community
  conference through-and-through, it has many similarities to the DEF CON of
  many years ago, but with a little more chill attitude.  BSides is a great
  opportunity for new speakers as well as those who want to interact with fellow
  hackers in a more chill and (slightly) smaller atmosphere -- though it's
  gotten quite busy itself over the years.  BSides takes over all the conference
  space at the Tuscany, and most of the hotel rooms, so it's a great opportunity
  to be completely immersed in the hacker scene.
  
- The [Diana Initiative](https://www.dianainitiative.org/) is "A conference
  committed to helping all those underrepresented in Information Security."  In
  the past, it's been a 1 day or 1/2 day affair, but now it's becoming a 2 day
  event, and I'm so happy to see such an important topic getting more love.
  
- [Ringzer0](https://ringzer0.training/) is a training-only event focusing
  predominantly on reverse engineering and exploitation.  It provides a nice
  alternative to Black Hat trainings (it's the same days, but an independent
  event).  The trainings offered here seem much more specific than Black Hat
  trainings, and I'm planning to take one, so I'll have a review here after the
  event.

### Planning

The biggest single piece of advice I can offer is: **don't try to do
everything**.  You can't do it, and managing your energy is actually an
important part of the week, *especially* if you're attending multiple of the
conferences during the week.

Beyond that, I encourage you to think about what you hope to get out of your
time.  If you'd like to try out contests, pick out one or maybe two and focus on
them.  If you're looking for a new role or wanting to meet new people, find
social opportunities.  If you're looking to expand your skills in a particular
direction, identify all of the relevant content in the area.

I've had years where I tried to do too much and ended the week feeling I'd done
nothing at all.  I typically prioritize interactive events -- contests, meeting
people, etc., -- over talks, because the talks will be recorded and available
later, unless the talk is something I plan to immediately apply.  At the bigger
events (DEF CON and Black Hat) the audience is likely to be so large that even
if you have questions, it will be hard to get them answered by the speaker.

## Logistics

Quite frankly, the best time to plan hotel and airfare has probably already
passed, but the 2nd best time to plan them is right now.  I expect both will
only rise in price from this point forward.  Unfortunately, prices have been
very volatile this summer.  As of writing, the following group rates for hotels
are still available:

- [DEF CON Room
  Block](https://book.passkey.com/gt/218227359?gtid=91d1496702985f29e56b82c5f9253b61)
  -- Note that this year, DEF CON is at **Caesar's Forum**, which is a new
  conference center located behind the Linq and Harrah's.  (It is attached to
  these two hotels by a skybridge.)
- [The
  Tuscany](https://res.windsurfercrs.com/ibe/details.aspx?propertyid=16539&checkin=08/07/2022&group=0822BSLV)
  is the off-strip resort that hosts BSidesLV.  They still have a number of
  rooms available, and most of the guests at the hotel will be fellow hackers
  during the course of the week.
- [Black Hat](https://book.passkey.com/go/bhusa2022) has rates at the Mandalay
  Bay.  I'd only recommend this if you'll be attending Black Hat, however, as
  it's at the far south end of the strip.
- [Ringzer0](https://book.passkey.com/e/50286476) has a special rate for those
  attending their training at Park MGM.  One feature of this hotel is that the
  entire thing is Non-Smoking.  Along with Vdara and the Delano, this is an
  unusual quality on the strip and great for those with allergies.

Airfare is obviously high dependant on where you are originating.  If it's not
too far and airfare looks a bit pricey for you, check out whether anyone from a
local DEF CON Group is driving and maybe you can split the gas and make a new
friend!  There's also ride and room share threads on the [DEF CON
Forum](https://forum.defcon.org/node/242080).  While there's obviously good
reasons to be careful of who you ride or room with, lots of people have had
success and met new friends along the way.

### Bringing Tech

Some people want to spend the whole week hacking.  Some want to be hands-off
keyboard the whole week.  You might be somewhere in between.  What you want to
do during the week will dictate a lot of the tech you bring with you.

Since I will be attending a training event and enjoy playing in the
contests/CTFs, I will necessarily be bringing a laptop with me -- in this case,
my [Framework Laptop](https://frame.work/) that I love.  (Full review of that
coming soon.)  I have a [1TB SSD](https://amzn.to/3oqgO4Y) which should be
enough for VMs for training and CTFs as well, but I'll probably also bring along
an [external SSD](https://amzn.to/3PxvISI) for sharing resources.  They're light
enough that the speed advantage over a typical flash drive is worth it.

If you do intend to take a training or play a CTF for more than a little bit, I
can't recommend a [wireless mouse](https://amzn.to/3z2dyBF) enough.  Even the
great trackpad on Macbooks just doesn't feel as good to me as a mouse after a
few hours.

Outlets can also be quite limited, so if you bring a [travel power
strip](https://amzn.to/3Puwb8t), you can always squeeze in where someone else
has plugged in and even provide more outlets.  Sharing is caring!

I'll also have my [Pixel 6 Pro](https://amzn.to/3zu5afS), but won't bring any
work tech along with me -- I'm fortunate to not be in an urgent/oncall role, and
this allows me to better focus on what I'm doing *there* instead of what's going
on in the office.  Though phone battery life has gotten pretty good for a lot of
phones, I'll still bring a [backup battery bank](https://amzn.to/3PNvp5Z).
There are even ones capable of [charging many laptops](https://amzn.to/3zqbwfY)
available, though they get a bit bulky and heavy.

I'll cover *protecting* your tech down below, but the short form is that I have
no problem bringing things (laptop, phone, etc.).

### Packing

Look, it's Las Vegas in August.  You don't need to check a weather forecast to
know that it's going to be *hot*.  Reaching 45&#x2103; (110&#x2109;) is not out
of the question.  There's not likely to be much rain, but I have seen it a time
or two.  Windy is a definite possibility though.  Dress accordingly.

In the casinos and the conference areas, the air conditioning is often on full
blast.  I'm personally comfortable in a T-Shirt and jeans or shorts, but if
you're prone to being cold under such conditions, a lightweight hoodie or jacket
might not be a bad idea.

I have two schools of thought on carrying things with me.  Some years, I have
intentionally used a [smaller backpack](https://amzn.to/3op9tCB) to avoid
lugging so much stuff around with me for days on end.  This does work out, but
then I end up wishing I had certain other items.  The other extreme is carrying
my [EDC backpack](https://amzn.to/3RYnN2o) full of gear and a sore back after a
couple of days.  Carrying the smaller backpack is probably the better decision,
but I can't say I'm always known for making the best decisions.

It may seem a bit anachronistic, but I also suggest carrying a [small
notebook](https://amzn.to/3vaoigl) (I'm quite partial to Field Notes with
Dot-Graph paper) and [pen](https://amzn.to/3aYapei).  To this day, I still find
it easier to make quick notes on pen and paper than on my phone, especially if I
need a diagram or drawing of any sort.  (It also requires no recharging.)

## Safety

### Stay Healthy

Addressing the elephant in the room, there is still a pandemic going on, and new
variants all the time.  Everyone has already made up their mind on vaccinations,
so I'm not going to try to push anyone on that, but I will strongly suggest
bringing some tests with you to Las Vegas.  If you test positive, *please* don't
come to the conferences and infect others.  Yes, missing out on part of con will
suck, but it's still the right thing to do.  DEF CON and BSidesLV are both
requiring masking at all times (consider [ear savers](https://amzn.to/3IXLILw)),
except when eating, drinking, or presenting.  Neither is requiring proof of
vaccination.

Even prior to the pandemic, Hacker Summer Camp posed its own health challenges.
Inadequate sleep is nearly universal, and drinking, heat, and dry air can
quickly lead to dehydration.  Drinking water is absolutely critical.  I strongly
recommend bringing an [insulated water bottle](https://amzn.to/3PNuanl), and you
can refill from water fountains in the conference space.  Bottled water in the
hotels is extremely expensive (I believe most people would call it a "rip-off")
but if you want to get bottled water, I suggest going to CVS, or the [ABC
convenience stores](https://abcstores.com/) on the Strip.  (Fun fact, those
stores also sell alcohol at pretty reasonable prices if you want to have a drink
in your room.  Hotel rules would definitely preclude carrying a
[flask](https://amzn.to/3ovCyfz) in the conference space, so no hackers would
ever do that.)

I particularly hate the heat, so I also bring a couple of ["cooling
towels"](https://amzn.to/3IYdvLI) -- you dampen them, and the evaporating water
causes them to cool off, consequently cooling you off.  They also make a great
basic towel for wiping sweat away or any other quick use.  I was skeptical when
I first heard of them, but they really work to make you feel cooler.

### Physical Safety

Las Vegas is a bit of a unique city in that it's built *entirely* around the
tourism industry.  This is even more true on or near "The Strip", the section of
Las Vegas Boulevard from The STRAT to Mandalay Bay (just north of Reid Airport).
Every scam you can imagine is being played here as well as many you won't even
have thought of.  Your Social Engineering instincts should be on high alert.

Pickpocketing and theft of anything unattended are both commonplace on the
strip, but robbery less so on the strip.  It's more your belongings than you
yourself that are at risk.  Stay in a group if you can.

Know that the street performers have an *expectation* of getting paid if you
take a photo with them.  This ranges from a guy in a poor Mickey Mouse costume
to women dressed up as Las Vegas showgirls.  It may get confrontational if you
take a photo and try not to tip them at all, but also don't let them rip you off
if you decide to do this.

### Electronic Safety

If you have fully up-to-date (patched) devices, I do not believe the risk of
compromise to be especially high.  Consider the value of 0-day exploits in
modern platforms along with the number of reverse engineers and malware analysts
present who might get a copy, resulting in the 0-day being "burned".  To the
best of my knowledge, no device I've ever taken has been compromised.  (And yes,
I used to take "burner" devices, my views on this have evolved over the years.)

If you have a device that *can't* run the latest available OS (i.e., no longer
receives Android or iOS Updates), I strongly recommend upgrading, whether or not
you plan to bring it to DEF CON.  Unfortunately there are enough browser and
similar bugs that affect older OSs that they're basically unsafe on *any* public
network, not just the ones at these conferences.

At DEF CON, they provide both an "open" network (on which there are plenty of
shenanigans, but not modern OS 0-day as far as I'm aware) and a "secure" network
that uses 802.1x authentication with certificates (make sure you verify the
network certificate) and also prevents client-to-client traffic.

I do recommend not bringing any particularly sensitive data, and having a
thorough backup before your trip.

VPNs are a bit of a controversial topic in the security space right now.  Too
many providers pretend they can offer things they can.  At a simple level, your
traffic is *eventually* egressing onto the public internet, and it's not
end-to-end encryption.  If you're in the security space and not familiar with
how commercial VPNs work, now might be a great time to look more into it.  I do
think they have value on open wireless networks because the opportunity for
meddler-in-the-middle attacks is less on a VPN than on the open WiFi.  I
personally use [Private Internet
Access](http://www.privateinternetaccess.com/pages/buy-a-vpn/1218buyavpn?invite=U2FsdGVkX18aKarfu8c-T4YL607CLgJXQ7bjtNnDbq8%2Cxx398ZSkDRCERKNANTr2j9zKN9M)
but there are many options out there.

## FAQs

### What's a Goon?

[DEF CON Goons](https://defcon.org/html/links/dc-goons.html) are the volunteer
army that help make sure DEF CON occurs as successfully and safely as possible.
While they have a bit of a reputation for their loudness and directness, their
goal is to keep things moving and do so safely.  They can be identified by their
red DEF CON badges.

### Where can I learn more about the history of DEF CON?

I'm hardly a historian, but I can recommend checking out the [DEF CON
documentary](https://www.youtube.com/watch?v=3ctQOmjQyYg) produced by Jason
Scott at DEF CON 20 in 2012.

### What is Badgelife?

The official DEF CON badges eventually inspired other creators to get into the
space of making badges as well.  These may be electronic, laser cut, hand
crafted, and more.  Some will be sold publicly, others are given out to friends,
and still others may be associated with an activity in one of the villages.
These are often called "unofficial badges" since they are not associated with
the DEF CON organizers and they **don't gain you access** to the conference.
(Some may gain you access to parties and events run by their creators, however.)

The electronic component shortage associated with the pandemic has slowed things
down a bit, but this space appears poised to make a come back this year or so.
At the end of the day, Badgelife is just a particularly nerdy form of art.
(I've been a small-volume badgelife creator for a few years, so I feel well
positioned to acknowledge the nerdiness.)

### Where Can I See Past Talks?

The [DEF CON Media Server](https://media.defcon.org/) has all the media from
every DEF CON held, but not every DEF CON had talks recorded.  Many of the
videos have also been [uploaded to
YouTube](https://www.youtube.com/user/DEFCONConference/videos?app=desktop).

Black Hat posts some of the videos from their conferences to their [YouTube
page](https://www.youtube.com/channel/UCJ6q9Ie29ajGqKApbLqfBOg).  Likewise,
BSidesLV has a [YouTube page](https://www.youtube.com/c/BsideslvOrg?app=desktop)
with their talks.  Finally, [The Diana
Initiative](https://www.youtube.com/c/TheDianaInitiative) has also uploaded
their videos from 2021.  (Though apparently none from before that time, at least
that I could locate.)

### What is the Rule on Photography?

Until about 10 years ago, the rule was *no photography allowed* but now that
basically everyone carries a camera with them wherever they go (my phone
actually has 4 separate cameras), it's been updated a bit:

Everyone in the photo *must consent* to having their photo taken at both DEF CON
and BSidesLV.  (And, quite frankly, this is good advice for life in general.)
This includes individuals in the background, etc.  There may also be areas
(Skytalks, Mohawkcon) that absolutely prohibit photography.  I have personally
witnessed individuals removed from events for violating this rule.

At DEF CON 15, an undercover reporter was [chased from the
event](https://www.wired.com/2007/08/media-mole-at-d/).  While the events do
allow press, they are required to register as such (which earns them a
specially-colored badge) and the policies require they identify themselves as
press to participants.

A reporter coming "undercover" hoping to catch individuals openly discussing
criming in the hallways is likely to be very disappointed.  You're far more
likely to catch people mocking the security industry itself.

### I Don't Know Anyone -- How Do I Meet People?!

I struggle with this myself, but the [Lonely Hackers
Club](https://lonelyhackers.club/) has a great
[guide](https://lonelyhackers.club/post/defconguide/).

## Closing

I hope some of these tips have been helpful to at least some of you.  :)  Feel
free to reach me on [Twitter](https://twitter.com/matir) with any feedback you
might have.  If you want to get into the right mindset, I highly recommend
checking out the music CDs or live recordings from [past
DEFCONs](https://media.defcon.org/) or checking out [Dual Core
Music](https://dualcoremusic.com/).
