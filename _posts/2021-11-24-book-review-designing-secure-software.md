---
layout: post
title: "Book Review: Designing Secure Software"
category: Security
date: 2021-11-24
tags:
  - Book Review
---

*Designing Secure Software* ([Amazon](https://amzn.to/3nRatAc),
[No Starch Press](https://nostarch.com/designing-secure-software)) by Loren Kohnfelder is
one of the latest entries in No Starch Press's line of security books.  This
book stands out to me for two big reasons.  First, this is one of the most
mindset-centric books I've seen (which means it is likely to age better than a
lot of more technically-specific books).  Second, this book caters to developers
more than security professionals (but don't take this to mean it's only for
developers), which is definitely a distinguishing feature from so many other
security books.

<!--more-->

**Note**: I was provided an early access copy of *Designing Secure Software* by the
publisher for review, but they had no editorial input.  All of the opinions in
this review are my own.

The writing in this book is very clear and easy reading, and the examples used
are both captivating and easy to understand.  Kohnfelder does a great job of
making a point that is easy to understand, and most of the chapters could stand
alone for developers just working in that one particular area.

Security is something that has to be baked into the software development life
cycle, so informing and educating developers is a key element of this.  This
book is a great resource for this and shows how vulnerabilites can creep in
during both the design and implementation phases of the SDLC.  There are
examples written in C and Python to help developers understand.

One of the best points made in this book is that security is a spectrum and that
we have to trust *something*.  Whether that's a compiler, an operating system
vendor, a cloud provider, or dependencies in your software's build process.
Sometimes people who find out a little bit about security become security
absolutists, looking for 100% guarantees, and while I recognize and understand
the instinct, the real world doesn't work that, and this book helps software
developers to understand and evaluate that.

Overall, this book is a worthwhile read for both software developers and
security engineers working the application security space.  It distinguishes
itself by focusing on concepts rather than being a checklist of individual items
to focus on.
